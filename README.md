OpenConnect Protocol Documentation
==================================

This project is about documenting the protocol used by the [Openconnect VPN](https://gitlab.com/openconnect/ocserv) client and server.

 - [Generated document from this repository](https://openconnect.gitlab.io/protocol/)
 - Current released document from IETF web site: [The OpenConnect VPN Protocol Version 1.2](https://datatracker.ietf.org/doc/html/draft-mavrogiannopoulos-openconnect-03)
